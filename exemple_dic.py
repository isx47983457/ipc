# /usr/bin/python
#-*- coding: utf-8-*-
# ERIC ESCRIBA
# Exemple dic
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
#parser.add_argument("-s","--sort",type=str,\
#        help="sort criteria: login | gid", metavar="criteria",\
#        choices=["login","gid","gname"],dest="criteri")
parser.add_argument("-g",type=str, help="group file (/etc/group)",dest="group", metavar="file")
parser.add_argument("-f",type=str,\
        help="user file (/etc/passwd style)", dest="usuaris",metavar="file")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
    self.gname=""
    if self.gid in groups_dic:
      self.gname=groups_dic[self.gid].gname
  def show(self):
    "Mostra les dades de l'usuari"
    print "login: %s, uid:%d, gid=%d , gname:%s" % \
           (self.login, self.uid, self.gid, self.gname)
  def sumaun(self):
    "funcio tonta que suma un al uid"
    self.uid+=1
  def __str__(self):
    "functió to_string"
    return "%s %d %d %s" % (self.login, self.uid, self.gid, self.gname)

class UnixGroup():
  '''
  Classe UnixGroup: prototipus /etc/group
  gname:passwd:gid:list_users
  '''
  def __init__(self,groupLine):
    groupField = groupLine.split(":")
    self.gname = groupField[0]
    self.passwd = groupField[1]
    self.gid = int(groupField[2])
    self.list_users = groupField[3]
    # Transformacio a una llista d'usuaris
    self.userlist = []
    if self.list_users[:-1]:
      self.userlist = self.list_users[:-1].split(",")
  def show(self):
    "Mostra les dades de l'usuari"
    print "gname:%s, gid:%d, userlist:%s" % (self.gname, self.gid, self.userlist)	
  def __str__(self):
    "Funció to string"
    return "%s %d %s" % (self.gname,self.gid,self.userlist)
		

# CREACIÓ DEL DICCIONARI AMB LA INFO DELS GIDS
groups_dic={}

fileGroup=open(args.group,"r")
for line in fileGroup:
	group=UnixGroup(line)
	groups_dic[group.gid]=group
	
fileGroup.close()

# CREACIÓ LLISTA USUARIS AMB ELS SEUS CAMPS
fileUsers=open(args.usuaris,"r")
userList=[]
for line in fileUsers:	
  user=UnixUser(line)
  userList.append(user)
  
  login = user.login
  gid = user.gid
	
  if login not in groups_dic[gid].userlist :
    groups_dic[gid].userlist.append(login)
    print login,groups_dic[gid].userlist,gid
    
fileUsers.close()

#for k in groups_dic:
#	print k,groups_dic[k]

