# /usr/bin/python
#-*- coding: utf-8-*-
#
# ERIC ESCRIBA
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# Exemple SIGNALS
# -------------------------------------

import sys,os,signal,argparse
parser = argparse.ArgumentParser(description='Exemple alarm')
parser.add_argument("alarm", help="segons")
args = parser.parse_args()

alarma = int(args.alarm)

pujar = 0
baixar = 0

def mydeath(signum,frame):
	print "Signal handler called with signal:", signum
	print "Bye Bye!"
	print pujar,baixar
	sys.exit(1)

def ignorar(signum,frame):
	print "Signal handler called with signal:", signum
	print "I IGNORE YOU!"
	

def incrementar(signum,frame):
	print "Signal handler called with signal:", signum
	signal.alarm(alarma + 60 )
	global baixar
	baixar += 1

def resta(signum,frame):
	signal.alarm(alarma -60 )
	global pujar
	pujar += 1
	
def mostrar(signum,frame):
	
	print signal.alarm(alarma)
	
	
def restart(signum,frame):
	alarm = alarma
	signal.alarm(alarm) 
	
signal.alarm(alarma)
# Ignora el Ctrl + C
signal.signal(signal.SIGINT,signal.SIG_IGN)

# SIGALRM plega
signal.signal(signal.SIGALRM,mydeath)

# SIGTERM mostra quan falta
signal.signal(signal.SIGTERM,mostrar)

# SIGHUP reinicialitza
signal.signal(signal.SIGHUP,restart)

# SIGUSR1 incrementa 1minut
signal.signal(signal.SIGUSR1,incrementar)

# SIGUSER2 resta 1 minut
signal.signal(signal.SIGUSR2,resta)

# Prog


print os.getpid()

while True:
	# Fem un open que es queda encallat
	pass



sys.exit(0)

