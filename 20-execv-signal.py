# /usr/bin/python
#-*- coding: utf-8-*-
#
# ERIC ESCRIBA
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# EXEMPLE EXECV
#----------------------------------------

import os,sys


print "Hola, begging of the program"
print "PID pare:", os.getpid()


pid = os.fork()

if pid != 0:
	print "Programa Pare",os.getpid(),pid
	sys.exit(0)


print "Programa Fill",os.getpid(),pid
os.execv("/usr/bin/python",["/usr/bin/python","16-alarm.py","180"])

print "Bye Bye"
sys.exit(0)
