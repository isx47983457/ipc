# /usr/bin/python
#-*- coding: utf-8-*-
#
# ERIC ESCRIBA
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# EXEMPLE FORKS
#----------------------------------------

import sys,os
print "Hola, begging of the program"
print "PID pare:", os.getpid()

# Es crea una copia del programa exactament igual
# El fork genera exactament el mateix proces 
pid = os.fork()

if pid != 0:
	print "Programa Pare",os.getpid(),pid
else:
	print "Programa Fill",os.getpid(),pid

print "Bye Bye"
sys.exit(0)
