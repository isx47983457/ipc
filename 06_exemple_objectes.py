#! /usr/bin/python
#-*- coding: utf-8-*-
# --------------------
# EXEMPLE DE CREACIÓ DE CLASSES I OBJECTES  
# 
# -----------------------

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,l,i,g):
    "Constructor objectes UnixUser"
    self.login=l
    self.uid=i
    self.gid=g
  def show(self):
    print "login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid)
  def sumaun(self):
    self.uid+=1
