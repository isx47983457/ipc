# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
###########################################


import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE


parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("-s","--server",type=str, default='')
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port

cmd = "ps ax"

pipeData = Popen(cmd,shell=True,stdout=PIPE)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

for line in pipeData.stdout:
	
	s.send(line)


s.close()
sys.exit(0)






