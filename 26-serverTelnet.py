# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Telnet Server""",\
        epilog="thats all folks")
parser.add_argument("-p","--port",type=int,\
        help="port del server", default=50001,dest="port")        
args=parser.parse_args()
llistaPeers=[]

HOST = ''
PORT = args.port


def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  print len(llistaPeers)
  sys.exit(0)

def mysigterm(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers, len(llistaPeers)
  sys.exit(0)
  
pid=os.fork()
if pid !=0:
  print "Engegat el server CAL:", pid
  sys.exit(0)
  
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm) 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()

while True:
  
  print "Connected by", addr
  
  while True:
    data = conn.recv(1024)
    pipeData = Popen(str(data),shell=True,stdout=PIPE)
    for line in pipeData.stdout:
	  conn.send(line)
	
    if not data: break
    conn.send(chr(4))
	
  conn.close()
