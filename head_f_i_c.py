#! /usr/bin/python
#-*- coding: utf-8-*-
# --------------------
# head [file]
# 5 lines, default=stdin
# -----------------------
import sys
import argparse
# ARGUMENT OPCIONAL -> Porta guió  "-f" 
# ARGUMENT POSICIONAL -> No porta guio "file"

parser = argparse.ArgumentParser(description="exemple de processar args",prog="exemple.py",epilog="thats all folks")

parser.add_argument("-n",type=int,help="linies (int)",dest="linies",choices=[5,10,15],metavar="n_linies 5|10|15",default=10)

parser.add_argument("-f",type=str,help="fitxer",dest="l_fit",metavar="elfitxer",action="append")

parser.add_argument("--verbosity",help="fer-ho verbose")

args=parser.parse_args()


def head(fit,linies):
	'''
	Funció que et mostra les n primers linies d'un fitxer
	Entrada: file, int
	Sortida: String
	'''
	ff = open(fit,"r")

	cont = 0
	for linia in ff:
		sys.stdout.write(linia)
		cont += 1
		if cont == linies: break
	
	ff.close()
	

for fitxer in args.l_fit:
	
	head(fitxer,args.linies)


exit(0)
