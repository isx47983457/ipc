# /usr/bin/python
#-*- coding: utf-8-*-
#
# ERIC ESCRIBA
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# EXEMPLE EXECV
#----------------------------------------

import os,sys


print "Hola, begging of the program"
print "PID pare:", os.getpid()


pid = os.fork()

if pid != 0:
	print "Programa Pare",os.getpid(),pid
	sys.exit(0)


print "Programa Fill",os.getpid(),pid
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
#os.execlp("ls","ls","-la","/")
#os.execve("/usr/bin/ls",["/usr/bin/ls","-la","/"], {"nom":"edat")

print "Bye Bye"
sys.exit(0)
