# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("sqlStatment",type=str,\
        help="directori a llistar")
args=parser.parse_args()
# -------------------------------------------------------

# command="select * from clientes;"

cmd = "psql -qtA -F',' -h 172.17.0.2 -U edtasixm06  training"
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(args.sqlStatment+"\n\q\n")

for line in pipeData.stdout:
  print line
sys.exit(0)

