# /usr/bin/python
#-*- coding: utf-8-*-
#
# ERIC ESCRIBA
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# Exemple SIGNALS
# -------------------------------------

import sys,os,signal

def myhandler(signum,frame):
	print "Signal handler called with signal:", signum
	print "Bye Bye!"
	sys.exit(1)

def mydeath(signum,frame):
	print "Signal handler called with signal:", signum
	print "You die!"
	

signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGUSR2,myhandler)
signal.signal(signal.SIGUSR1,mydeath)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(170)
print os.getpid()

while True:
	# Fem un open que es queda encallat
	pass


signal.alarm(60)
sys.exit(0)
