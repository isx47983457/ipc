# /usr/bin/python
#-*- coding: utf-8-*-
#
# ERIC ESCRIBA
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# EXEMPLE FORKS
#----------------------------------------

import sys,os,signal

def hello(signum,frame):
	print "hello"

def mydeath(signum,frame):
	print "Signal handler called with signal:", signum
	print "Bye Bye!"
	sys.exit(1)

print "Hola, begging of the program"
print "PID pare:", os.getpid()

signal.signal(signal.SIGUSR1,hello)
signal.signal(signal.SIGUSR2,mydeath) 

# Es crea una copia del programa exactament igual
# El fork genera exactament el mateix proces 
pid = os.fork()

if pid != 0:
	print "Programa Pare",os.getpid(),pid
	print "Bye Bye"
	sys.exit(1)


print "Programa Fill",os.getpid(),pid
while True:
	pass
	

sys.exit(0)
