# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """CAL server""",\
        epilog="thats all folks")
parser.add_argument("-a","--any",type=int,\
        help="any del cal", default=2019,dest="year")
parser.add_argument("-p","--port",type=int,\
        help="port del server", default=50001,dest="port")        
args=parser.parse_args()


llistaPeers=[]

def mysigusr1(signum,frame):
  global upp
  print "Signal handler called with signal:", signum
  print "Hola radiola"
  print llistaPeers
  sys.exit(0)

def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  print "Adeu andreu!"
  print len(llistaPeers)
  sys.exit(0)
 
def mysigterm(signum,frame):
	print "SIgnal handler called with signal:",signum
	print llistaPeers,len(llistaPeers)
	sys.exit(0)
    

pid=os.fork()

if pid !=0:
  print "Engegat el server cal",pid
  print "Hasta lugo lucas!"
  sys.exit(0)
 


signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)


HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print "Connected by", addr
  llistaPeers.append(addr)
  command = ["cal %d" % (args.year)]
  pipeData = Popen(command,stdout=PIPE,shell=True)
  for line in pipeData.stdout:
    conn.send(line)
  conn.close()



