# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("-d",type=str,\
		help="database",default="training",dest="data_base")
parser.add_argument("-c",type=str,\
        help="directori a llistar",dest="llista",action="append",required=True)
args=parser.parse_args()
# -------------------------------------------------------

cmd = "psql -qtA -F',' -h 172.17.0.2 -U postgres %s" % (args.data_base)
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.llista:
	consulta = ("select * from clientes where num_clie = " + num_clie + ";")
	pipeData.stdin.write(consulta + "\n")
	print pipeData.stdout.readline()


pipeData.stdin.write("\q\n")
sys.exit(0)

