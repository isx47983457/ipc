# /usr/bin/python
#-*- coding: utf-8-*-
#
# list-users [-f file]
# 10 lines, file o stdin
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
parser.add_argument("-f","--fit",type=str,\
        help="user file or stdin (/etc/passwd style)", metavar="file",\
        default="/dev/stdin",dest="fitxer")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print "login: %s, uid:%d, gid=%d" % \
           (self.login, self.passwd, self.uid, self.gid,\
            self.gecos, self.home, self.shell)
  def sumaun(self):
    "funcio tonta que suma un al uid"
    self.uid+=1
  def __str__(self):
    "functió to_string"
    return "%s %d %d" % (self.login, self.uid, self.gid)
# -------------------------------------------------------
fileIn=open(args.fitxer,"r")
userList=[]
for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
fileIn.close()
print userList
for user in userList:
 print user
exit(0)

