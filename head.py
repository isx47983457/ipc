#! /usr/bin/python
#-*- coding: utf-8-*-
# --------------------
# head [file]
# 5 lines, default=stdin
# -----------------------

import sys


linies = 5
fileIn = sys.stdin

if len(sys.argv) == 2:
	fileIn = open(sys.argv[1],"r")

cont = 0
for linia in fileIn:
	sys.stdout.write(linia)
	cont += 1
	if cont == linies: break
	
	
fileIn.close()

exit(0)
