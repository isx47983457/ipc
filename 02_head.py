#! /usr/bin/python
#-*- coding: utf-8-*-
# --------------------
# head [file]
# 5 lines, default=stdin
# -----------------------

import argparse

parser = argparse.ArgumentParser(description="exemple de processar args",prog="exemple.py",epilog="thats all folks")

parser.add_argument("-e","--edat",type=int,help="edat (int)",dest="useredat",metavar="laedat")

parser.add_argument("fit",type=str,help="fitxer",metavar="elfitxer")

parser.add_argument("--verbosity",help="fer-ho verbose")

args=parser.parse_args()

print parser
print args

exit(0)
